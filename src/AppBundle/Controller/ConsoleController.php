<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ConsoleController extends Controller
{
    /**
     * @Route("/console", name="console")
     */
    public function ConsoleAction(Request $request) {
        $url = $request->getSchemeAndHttpHost();
        if (preg_match("/^http\:/", $url)) {
            $url = preg_replace("/http/", "https", $url);
        }
        if (preg_match("/\:[0-9]{1,5}$/", $url)) {
            $url = preg_replace("/\:[0-9]{1,5}$/", "", $url);
        }
        return $this->render('shell/index.html.twig', [
            'url' => $url
        ]);
    }

}
