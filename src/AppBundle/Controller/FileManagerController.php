<?php

/**
 * == ARGON ==
 * --------------------------------------
 * CONTROLADOR DEL EXPLORADOR DE FICHEROS
 * --------------------------------------
 * Las secciones generales se controlarán des de aquí.
 * Cada sección que requiera controladores específicos los tendrá en la misma carpeta
 * en la que se halle ESTE controlador.
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Users;

class FileManagerController extends Controller {
    /**
     * @Route("/explorer", name="explorer")
     */
    public function ExplorerAction(Request $request) {
        return $this->render("explorer/index.html.twig");
    }

    /**
     * @Route("/explorer/list.json", name="explorer_ls")
     */
    public function ExplorerListAction(Request $request) {
        // sleep(2);
        $dir = $request->query->get("dir");
        if (empty($dir)) {
            $response = new Response(json_encode(["error" => "missing params: dir"]));
            $response->headers->set("Content-Type", "application/json");
            return $response;
        }
        if (substr($dir, -1) != "/") {
            $dir .= "/";
        }
        try {
            $content = scandir($dir);
            array_shift($content); // elimina "."
            array_shift($content); // elimina ".."
            $res = [];
            foreach ($content as $file) {
                $path = $dir . $file;
                $flag = "desconocido";
                $size = "";
                $umask = "";
                $owner = "";
                if (is_file($path)) {
                    try {
                        $flag = mime_content_type($path);   
                        $umask = substr(sprintf('%o', fileperms($path)), -4); 
                        $owner = posix_getpwuid(fileowner($path))['name'] . ":" . posix_getgrgid(filegroup($path))['name'];
                    } catch (\Exception $e) {
                        $flag = "unknown file";
                        $umask = "----";
                        $owner = "?:?";
                    }
                    
                    $size = $this->GetHumanReadableFileSize(filesize($path));
                }
                if (is_dir($path)) {
                    $flag = "directory";
                    $size = "---";
                    $umask = substr(sprintf('%o', fileperms($path)), -4); 
                    $owner = posix_getpwuid(fileowner($path))['name'] . ":" . posix_getgrgid(filegroup($path))['name'];
                    $size = "4.00 KB"; // las carpetas siempre reportan ocupar 4 KB a menos que no se tenga permiso para leerlas
                }
                if (is_link($path)) {
                    $flag = "link";
                    $size = $this->GetHumanReadableFileSize(filesize($path));
                    $umask = substr(sprintf('%o', fileperms($path)), -4); 
                    $owner = posix_getpwuid(fileowner($path))['name'] . ":" . posix_getgrgid(filegroup($path))['name'];
                    $size = $this->GetHumanReadableFileSize(filesize($path));
                    $linkData = readlink($path);
                }
                $res[] = [
                    'name' => $file,
                    'type' => $flag,
                    'size' => $size,
                    'umask' => $umask, 
                    'owner' => $owner,
                    'link' => (is_link($path)) ? $linkData : "",
                ];

            }
            $response = new Response(json_encode($res), 200);
            $response->headers->set("Content-Type", "application/json");
            return $response;
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            if (preg_match("/Permission denied/", $msg)) {
                $error = "Permiso denegado";
            } elseif (preg_match("/Not a directory/", $msg)) {
                $error = "Esto no parece un directorio";
            } else {
                $error = $msg;
            }
            $response = new Response(json_encode(["error" => $error]), 500);
            $response->headers->set("Content-Type", "application/json");
            return $response;
        }
    }

    /**
     * @Route("/explorer/get.json", name="explorer_cat")
     */
    public function FileShowAction(Request $request) {
        // json$|^text
        $file = $request->query->get("file");
        if (is_dir($file)) {
            $res = new Response(json_encode([
                "status" => "error",
                "message" => "this is a directory"
            ]), 403);
            $res->headers->set("Content-Type", "application/json");
            return $res;
        }
        if (file_exists($file)) {
            try {
                $res = new Response(json_encode([
                    "status" => "ok",
                    "path" => $file,
                    "content" => file_get_contents($file)
                ]), 200);
            } catch (\Exception $e) {
                $res = new Response(json_encode([
                    "status" => "error",
                    "message" => "internal server error"
                ]), 500);
                $res->headers->set("Content-Type", "application/json");
                return $res;
            }
        } else {
            $res = new Response(json_encode([
                "status" => "error",
                "message" => "not found"
            ]), 404);
        }
        $res->headers->set("Content-Type", "application/json");
        return $res;
    }

    // credit: http://jeffreysambells.com/2012/10/25/human-readable-filesize-php
    private function GetHumanReadableFileSize($bytes) {
        $size = array('B','KB','MB','GB','TB','PB','EB','ZB','YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        $val = sprintf("%.2f", $bytes / pow(1024, $factor)) . " " . @$size[$factor];
        if ($val == "0.00 B") {
            return "Vacío";
        } else {
            return $val;
        }
    }
}
