<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Logs;

class LogViewerController extends Controller {

    /**
     * @Route("/api/logs.json", name="logs")
     */
    public function LogsAction(Request $request) {
        $limit = $request->query->get("limit");

        $repository = $this->getDoctrine()
            ->getRepository(Logs::class);

        $qb = $repository->createQueryBuilder('l')
            ->orderBy('l.id', 'DESC')
            ->setMaxResults($limit)
            ->getQuery();

        $logs = $qb->getArrayResult();
        
        $res = new Response(json_encode($logs), 200);
        $res->headers->set("Content-Type", "application/json");
        return $res;
    }

    /**
     * @Route("/api/os.json", name="uname")
     */
    public function OSInfoAction() {
        $kernel = shell_exec("uname");
        $kernel_ver = shell_exec("uname -r");
        $arch = shell_exec("uname -i");
        $os = shell_exec("uname -o");
        $_lsb = shell_exec("lsb_release -a");
        $_lsb = preg_replace("/\t/", "", $_lsb);
        $_lsb = explode("\n", $_lsb);
        array_pop($_lsb);
        $res = [
            'os' => $os,
            'kernel' => $kernel,
            'version' => $kernel_ver,
            'arch' => $arch
        ];
        foreach ($_lsb as $key=>$item) {
            $tmp = explode(":", $item);
            $tmp[0] = preg_replace("/\s/", "", $tmp[0]);
            if ($tmp[0] == "Description") {
                $res["desc"] = $tmp[1];
            }
        }

        $response = new Response(json_encode($res), 200);
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }
}