<?php

/**
 * == ARGON ==
 * ---------------------
 * CONTROLADOR PRINCIPAL
 * ---------------------
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use AppBundle\Entity\Users;
use AppBundle\Entity\Roles;
use AppBundle\Entity\Settings;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function HomeAction(Request $request) {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/resources/avatar", name="avatar")
     */
    public function AvatarAction(Request $request) {
        $url = $request->getSchemeAndHttpHost();
        $user = $this->getUser();
        if (file_exists("./assets/img/avatars/" . $user->getUsername())) {
            return new Response($url . "/assets/img/avatars/" . $user->getUsername());
        } else {
            return new Response($url . "/assets/img/avatar.png");
        }
    }

    /**
     * @Route("/install", name="install")
     */
    public function InstallAction(UserPasswordEncoderInterface $passwordEncoder) {
        $repo = $this->getDoctrine()->getRepository(Users::class);
        $users = $repo->findAll();
        if (count($users) == 0) { // no hay usuarios, suficiente para re-instalar
            echo "<p style=\"font-family: sans-serif\">Instalando...</p>";
            $em = $this->getDoctrine()->getManager();
            // roles
            $admin = new Roles();
            $admin->setId(1)
                ->setRole("ROLE_ADMIN")
                ->setName("Administrador");
            $tech = new Roles();
            $tech->setId(2)
                ->setRole("ROLE_TECH")
                ->setName("Informático");
            $em->persist($admin);
            $em->persist($tech);
            // settings
            $s1 = new Settings();
            $s1->setId("agent_secret")
                ->setValue("0863763fb98c2881baddb913503f012980e97674cdf149428c7597");
            $s2 = new Settings();
            $s2->setId("show_phones")
                ->setValue("true");
            $s3 = new Settings();
            $s3->setId("tg_token")
                ->setValue("");
            $s4 = new Settings();
            $s4->setId("tg_group")
                ->setValue("");
            $em->persist($s1);
            $em->persist($s2);
            $em->persist($s3);
            $em->persist($s4);
            // usuarios
            $user = new Users();
            $user->setUsername("argon")
                ->setPassword($passwordEncoder->encodePassword($user, "argon"))
                ->setEmail("argon@argon.local")
                ->setName("Argon Admin")
                ->setPhone("")
                ->setRole($admin);
            $rolesRepo = $this->getDoctrine()->getRepository('AppBundle:Roles');
            $roles = $rolesRepo->findAll();
            
            $em->persist($user);
            $em->flush();
            echo "<br/><p style=\"font-family: sans-serif\">Instalado con éxito! Puedes ir a la <a href=\".\">página principal</a>.</p>";
        } else {
            echo "<p style=\"font-family: sans-serif\">Argon ya está instalado completamente.</p>";
        }
        return new Response("");
    }
}
