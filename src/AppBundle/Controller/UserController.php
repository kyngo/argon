<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use AppBundle\Entity\Users;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use AppBundle\Services\LogWriter;

class UserController extends Controller {
    /**
     * @Route("/me", name="me")
     */
    public function MyProfileAction() {
        return $this->render('account/me.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/me/update", name="update_me")
     */
    public function UpdateAccountAction(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $uid = intval($request->request->get("user_id"));
        $usersRepo = $this->getDoctrine()->getRepository('AppBundle:Users');
        $user = $usersRepo->findById($uid)[0];
        $rolesRepo = $this->getDoctrine()->getRepository('AppBundle:Roles');
        $roles = $rolesRepo->findAll();
        foreach ($roles as $role) {
            if ($role->getRole() == "ROLE_ADMIN") {
                $role_admin = $role;
            } else {
                $role_tech = $role;
            }
        }

        $alias = $request->request->get("userAlias");
        $phone = $request->request->get("userPhone");
        $passwd = $request->request->get("newPassword");
        $passwd_c = $request->request->get("newPasswordConfirm");
        $mail = $request->request->get("userEmail");
        $admin = $request->request->get("isAdmin");

        if (!empty($alias)) {
            $user->setName($alias);
        }

        if (!empty($phone)) {
            $user->setPhone($phone);
        }
        
        if (!empty($passwd)) {
            if ($passwd == $passwd_c) {
                $user->setPassword($passwordEncoder->encodePassword($user, $passwd));
            }
        }

        $user->setEmail($mail);
        
        if ($this->getUser()->getId() != $user->getId()) {
            if ($admin == "true") {
                $user->setRole($role_admin);
                $logs = new LogWriter($this->getDoctrine()->getManager());
                $logs->FromInternalCall(
                    "Nombrando/editando a un administrador:  " . $alias
                );
            } else {
                $user->setRole($role_tech);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        $res = new Response(json_encode(["code" => "ok"]), 200);
        $res->headers->set("Content-Type", "application/json");
        return $res;
    }

    /**
     * @Route("/me/adduser", name="adduser")
     */
    public function AddUserAction(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $user = new Users();
        
        $username = $request->request->get('username');
        $password = $passwordEncoder->encodePassword($user, $request->request->get("password"));
        $email = $request->request->get('email');
        $name = $request->request->get('name');
        $phone = $request->request->get('phone');
        $admin = $request->request->get('admin');

        $user->setUsername($username)
            ->setPassword($password)
            ->setEmail($email)
            ->setName($name)
            ->setPhone($phone);

        $rolesRepo = $this->getDoctrine()->getRepository('AppBundle:Roles');
        $roles = $rolesRepo->findAll();
        foreach ($roles as $role) {
            if ($role->getRole() == "ROLE_ADMIN") {
                $role_admin = $role;
            } else {
                $role_tech = $role;
            }
        }
        
        if ($admin == "true") {
            $user->setRole($role_admin);
        } else {
            $user->setRole($role_tech);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        
        $logs = new LogWriter($this->getDoctrine()->getManager());
        $logs->FromInternalCall(
            "Usuario " . $username . " creado"
        );
        return new Response("¡Usuario añadido!");
    }

    /**
     * @Route("/me/edit", name="edituser")
     */
    public function EditUserAction(Request $request, UserPasswordEncoderInterface $passwordEncoder) {
        $query = $request->query->get("query");
        if ($query != "") { // obtener los datos de un usuario
            $repo = $this->getDoctrine()->getRepository('AppBundle:Users');
            $user = $repo->findById($query);
            return $this->render("account/me.html.twig", [
                'user' => $user[0]
            ]);
        } else {
            return $this->render("account/me.html.twig", [
                'user' => $this->getUser()
            ]);
        }
    }

    /**
     * @Route("/me/delete", name="deluser")
     */
    public function DeleteUserAction(Request $request) {
        try {
            $_user = $request->query->get("user");
            if ($_user == $this->getUser()->getId()) {
                throw new \Exception("¡No puedes borrarte a ti mismo!");
            }
            $repo = $this->getDoctrine()->getRepository('AppBundle:Users');
            $user = $repo->findById($_user);
            $em = $this->getDoctrine()->getManager();
            $em->remove($user[0]);
            $em->flush();
            $avatar = "./assets/img/avatars/" . $user[0]->getUsername();
            if (is_file($avatar)) {
                unlink("./assets/img/avatars/" . $user[0]->getUsername());
            }
            $logs = new LogWriter($this->getDoctrine()->getManager());
            $logs->FromInternalCall(
                "Usuario " . $user[0]->getUsername() . " eliminado"
            );
            return new Response("¡Usuario eliminado!", 200);
        } catch (\Exception $e) {
            return new Response("Ha ocurrido un error.", 500);
        }
        
    }

    /**
     * @Route("/me/avatar", name="avatar_upload")
     */
    public function AvatarUploadAction(Request $request) {
        $res = new Response();
        $res->headers->set("Content-Type", "application/json");
        $repo = $this->getDoctrine()->getRepository('AppBundle:Users');
        $user = $this->getUser()->getUsername();
        if ($request->query->get("tmp") == "true") {
            $path = "tmp_avt";
            try {
                $file = $_FILES['file'];
                // credito: https://stackoverflow.com/questions/9314164/php-uploading-files-image-only-checking
                $info = getimagesize($_FILES['file']['tmp_name']);
                if ($info === FALSE) {
                    $res->setContent(json_encode(["code" => "error", "msg" => "No se pudo determinar el tipo de imagen"]), 400);
                }
                if (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
                    $res->setContent(json_encode(["code" => "error", "msg" => "No es un formato válido de imagen"]), 400);
                }
                move_uploaded_file($_FILES['file']['tmp_name'], "./assets/img/" . $path . "/" . $user);
                $res->setContent(json_encode(["code" => "ok"]), 200);
            } catch (\Exception $e) {
                $res->setContent(json_encode(["code" => "error", "msg" => $e]), 500);
            }
        } else {
            $path = "avatars";
            $original = "./assets/img/tmp_avt/" . $user;
            $x = $request->request->get("x");
            $y = $request->request->get("y");
            $width = $request->request->get("width");
            $height = $request->request->get("height");
            list($old_width, $old_height) = getimagesize($original);

            // detectamos el formato de imagen
            $orig = null;
            if (exif_imagetype($original) == IMAGETYPE_PNG) {
                $orig = imagecreatefrompng($original);
            }
            if (exif_imagetype($original) == IMAGETYPE_JPEG) {
                $orig = imagecreatefromjpeg($original);
            }
            if (exif_imagetype($original) == IMAGETYPE_GIF) {
                $orig = imagecreatefromgif($original);
            }
            $dest = imagecrop(
                $orig,
                ['x' => $x, 'y' => $y, 'width' => $width, 'height' => $height]
            );
            imagejpeg($dest, "./assets/img/avatars/" . $user);
            $res->setContent(json_encode(["code" => "ok"]), 200);
        }
        return $res;
    }
}
