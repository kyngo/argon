<?php


namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Users;

class MonitorController extends Controller {
    /**
     * @Route("/monitor", name="monitor")
     */
    public function MonitorAction() {
        return $this->render("monitor/index.html.twig");
    }

    /**
     * @Route("/monitor/settings.json", name="monitor_config")
     */
    public function MonitorSettingsAction(Request $request) {
        $cores = shell_exec("nproc");
        $ram = $this->GetRAM()['total'];
        $swap = $this->GetRAM()['swap_total'];
        $response = new Response(json_encode([
            'cores' => intval($cores),
            'ram' => intval($ram),
            'disks' => count($this->GetDisks()),
            'public_ip' => $this->GetPublicIP(),
            'swap' => intval($swap),
        ]), 200);
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }

    /**
     * @Route("/monitor/get.json", name="monitor_ls")
     */
    public function MonitorGetAction(Request $request) {
        $stat1 = $this->GetCoreInformation();
        usleep(850000);
        $stat2 = $this->GetCoreInformation();
        $res = [
            'cpu_usage' => $this->GetCpuPercentages($stat1, $stat2),
            'ram' => $this->GetRAM(),
            'disks' => $this->GetDisks(),
            'network' => $this->GetNetworkInfo()
        ];
        $response = new Response(json_encode($res), 200);
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }

    // obtiene la ram del sistema
    private function GetRAM() {
        $cmd = shell_exec('free');
        $cmd = (string)trim($cmd);
        $arr = explode("\n", $cmd);
        $mem = explode(" ", $arr[1]);
        $swap = explode(" ", $arr[2]);
        $mem = array_filter($mem);
        $swap = array_filter($swap, function($value) { return $value !== ''; });
        $mem = array_merge($mem);
        $swap = array_merge($swap);
        return ['total' => $mem[1], 'used' => $mem[2], 'swap_total' => $swap[1], 'swap_used' => $swap[2]];
    }

    // obtiene los discos del sistema y su uso
    private function GetDisks() {
        $res = [];
        $cmd = shell_exec("df");
        $arr = explode("\n", $cmd);
        array_shift($arr);
        array_pop($arr);
        foreach ($arr as $k=>$item) {
            $_tmp = preg_replace("/\s+/", " ", $item);
            $tmp = explode(" ", $_tmp);
            if (preg_match("/^\//", $tmp[0]) && !preg_match("/loop/", $tmp[0])) {
                $res[] = [
                    'device' => $tmp[0],
                    'size' => $tmp[1],
                    'used' => $tmp[2],
                    'free' => $tmp[3],
                    'mountpoint' => $tmp[5]
                ];
            }
        }
        return $res;
    }

    // obtiene info de la cpu
    // credito: https://gist.github.com/rlemon/1780212
    private function GetCoreInformation() {
        $data = file('/proc/stat');
        $cores = array();
        foreach( $data as $line ) {
            if (preg_match('/^cpu[0-9]/', $line)) {
                $info = explode(' ', $line);
                $cores[] = array(
                    'user' => $info[1],
                    'nice' => $info[2],
                    'sys' => $info[3],
                    'idle' => $info[4]
                );
            }
        }
        return $cores;
    }

    // credito: https://gist.github.com/rlemon/1780212
    private function GetCpuPercentages($stat1, $stat2) {
        if( count($stat1) !== count($stat2) ) {
            return;
        }
        $cpus = array();
        for( $i = 0, $l = count($stat1); $i < $l; $i++) {
            $dif = array();
            $dif['user'] = $stat2[$i]['user'] - $stat1[$i]['user'];
            $dif['nice'] = $stat2[$i]['nice'] - $stat1[$i]['nice'];
            $dif['sys'] = $stat2[$i]['sys'] - $stat1[$i]['sys'];
            $dif['idle'] = $stat2[$i]['idle'] - $stat1[$i]['idle'];
            $total = array_sum($dif);
            $cpu = array();
            foreach($dif as $x=>$y) {
                $cpu[$x] = round($y / $total * 100, 1);
            }
            $cpus['cpu' . $i] = $cpu;
        }
        return $cpus;
    }

    // obtiene información de la red del sistema
    private function GetNetworkInfo() {
        $cmd = shell_exec("ip addr");
        // $_tmp = preg_replace("/\s+/", " ", $cmd);
        $_tmp = preg_split("/\n[0-9]\:/", $cmd);
        $_tmp[0] = preg_replace("/^1\:/", "", $_tmp[0]);
        $res = [];
        foreach ($_tmp as $key=>$_item) {
            $_tmp[$key] = preg_replace("/^\s/", "", $_tmp[$key]);
            $_item = explode(":", $_item);
            $iface = preg_replace("/^\s/", "", $_item[0]);
            $ipv4 = shell_exec("ip addr show " . $iface . " | grep 'inet ' | cut -f 6 -d' '");
            $ipv4 = preg_replace("/\n/", "", $ipv4);
            $ipv6 = shell_exec("ip addr show " . $iface . " | grep 'inet6 ' | cut -f 6 -d' '");
            $ipv6 = preg_replace("/\n/", "", $ipv6);
            $res[] = [
                'interface' => $iface,
                'ipv4' => $ipv4,
                'ipv6' => $ipv6,
            ];
        }
        return $res;
    }

    private function GetPublicIP() {
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => 'https://kyngo.es/ip/json',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true
        ]);
        $res = curl_exec($ch);
        return json_decode($res, true)['ip'];
    }
}