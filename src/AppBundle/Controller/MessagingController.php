<?php

/**
 * == ARGON ==
 * -------------------------
 * CONTROLADOR DE MENSAJERÍA
 * -------------------------
 * Cada vez que Argon reciba una solicitud a su API de mensajería será procesada aquí.
 * Los mensajes serán enviados respectivamente por Telegram y/o correo electrónico
 * según la configuración del sistema.
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Services\Mailer;
use AppBundle\Services\Telegram;
use AppBundle\Services\SettingsService;
use AppBundle\Services\LogWriter;

class MessagingController extends Controller {

    /**
     * @Route("/api/report", name="reporter")
     */
    public function ReporterAction(Request $request, Mailer $mailer) {
        $settings = new SettingsService();
        $settings = $settings->Get($this->getDoctrine()->getRepository('AppBundle:Settings'));
        $headers = $request->headers->all();

        if ((isset($headers["x-argon-secret"])) && ($headers["x-argon-secret"][0] == $settings['agent_secret'])) {
            // Obtención de los usuarios
            $usersRepo = $this->getDoctrine()->getRepository('AppBundle:Users');
            $users = $usersRepo->findAll();
            $to = [];
            foreach ($users as $user) {
                $to[] = $user->getEmail();
            }
            // Procesado de los mensajes
            $params = json_decode($request->request->get("msg"), true);
            $text = [];
            $cat = [];
            foreach ($params as $item) {
                $text[] = $item[1];
                $cat[$item[1]] = $item[0];
            }
            // Obtención del hostname
            $hostname = preg_replace("/\n/", "", shell_exec("hostname"));
            $res = new Response();
            $res->headers->set("Content-Type", "application/json");
            if ($text != []) {
                if (isset($settings['tg_token']) && isset($settings['tg_group'])) {
                    // Telegram
                    $tg_text = "
"; // salto de linea necesario
                    foreach ($text as $item) { // cada item con un emoji de un punto delante
                        $tg_text .= "🔘 " . $item . "
"; // salto de linea necesario
                    }
                    $tg = new Telegram($settings);
                    $tg_msg = $tg->Send("<i>" . $hostname . "</i>: " . $tg_text);
                } else {
                    $tg_msg = false;
                }
                // Correo electrónico
                $mail_msg = $mailer->Send($to, "Información de Argon (" . $hostname . ")", $text);
                $mail_msg = true;
                // Logger de Argon
                $lr = new LogWriter($em = $this->getDoctrine()->getManager());
                foreach ($text as $item) {
                    $lr->FromAPICall($cat[$item], $item);
                }
                // Response
                $res->setContent(json_encode(['status' => 'reported', 'tg' => $tg_msg, 'mail' => $mail_msg]));
                return $res;   
            } else {
                $res->setContent(json_encode(['status' => 'notified']));
                return $res;   
            }
        } else {
            $res = new Response(json_encode(["error" => "secret not valid or missing"]), 401);
            $res->headers->set("Content-Type", "application/json");
            return $res;
        }
    }
}