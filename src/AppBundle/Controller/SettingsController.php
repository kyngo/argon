<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use AppBundle\Entity\Users;
use AppBundle\Entity\Settings;
use AppBundle\Services\SettingsService;
use AppBundle\Services\Telegram;

class SettingsController extends Controller {
    /**
     * @Route("/settings", name="settings")
     */
    public function SettingsAction(Request $request) {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        
        $repository = $this->getDoctrine()->getRepository('AppBundle:Users');
        $users = $repository->findAll();
        $users = $serializer->serialize($users, 'json');
        $users = json_decode($users, true);

        $settings = new SettingsService();
        $settings = $settings->Get($this->getDoctrine()->getRepository('AppBundle:Settings'));

        return $this->render("settings/index.html.twig", [
            'users' => $users,
            'settings' => $settings
        ]);
    }

    /**
     * @Route("/settings/update", name="settings_update")
     */
    public function SettingsUpdateAction(Request $request) {
        $res = new Response();
        $res->headers->set("Content-Type", "application/json");
        try {
            $_tg_api = $request->request->get("tg_token");
            $_tg_group = $request->request->get("tg_group");
            $_show_phones = $request->request->get("show_phones");
    
            $repository = $this->getDoctrine()->getRepository('AppBundle:Settings');
            $old_tg_api = $repository->findById('tg_token')[0]->getValue();
            $old_tg_group = $repository->findById('tg_group')[0]->getValue();
            $tg_api = $repository->findById('tg_token')[0]->setValue($_tg_api);
            $tg_group = $repository->findById('tg_group')[0]->setValue($_tg_group);
            $show_phones = $repository->findById('show_phones')[0]->setValue($_show_phones);
            $em = $this->getDoctrine()->getManager();
            $em->persist($tg_api);
            $em->persist($tg_group);
            $em->persist($show_phones);
            $em->flush();
            if ($_tg_api != "" && $_tg_group != "") {
                if ($_tg_api != $old_tg_api || $_tg_group != $old_tg_group) {
                    $settings = new SettingsService();
                    $settings = $settings->Get($this->getDoctrine()->getRepository('AppBundle:Settings'));
                    $tg = new Telegram($settings);
                    $hostname = preg_replace("/\n/", "", shell_exec("hostname"));
                    $tg_msg = $tg->Send("<i>" . $hostname . "</i>: Si recibes esto, has configurado bien el bot!");
                }
            }
            $res->setContent(json_encode(["status" => "¡Actualizado con éxito!"]));
        } catch (\Exception $e) {
            $res->setContent(json_encode(["status" => "error " . $e]));
        }
        return $res;
        
    }
}
