<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use AppBundle\Entity\Users;
use AppBundle\Entity\Roles;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use AppBundle\Services\LogWriter;
use AppBundle\Services\SettingsService;

class AuthenticationController extends Controller {
    /**
     * @Route("/login", name="login")
     */
    public function LoginAction(Request $request, AuthenticationUtils $authUtils) {
        $repo = $this->getDoctrine()
            ->getRepository(Users::class);
        $users = $repo->findAll();
        if (count($users) == 0) {
            return new Response("<p style=\"font-family: sans-serif\"><a href=\"./install\">Instalar</a> Argon</p>");
        }
        $settings = new SettingsService();
        $settings = $settings->Get($this->getDoctrine()->getRepository('AppBundle:Settings'));
        if( TRUE === $this->get('security.authorization_checker')->isGranted('ROLE_USER') ){
            return $this->render('security/login.html.twig', [
                'error' => [
                    'messageKey'    => '¡Ya has iniciado sesión!',
                    'messageData'   => [],
                    'users'         => $users,
                    'settings'      => $settings
                ]
            ]);
        }
        // si hay un error al autenticarse, se guardará aquí
        $error = $authUtils->getLastAuthenticationError();
        // el último usuario introducido, si lo hay
        $lastUsername = $authUtils->getLastUsername();

        $logs = new LogWriter($this->getDoctrine()->getManager());
        if ($error != null) {
            $logs->FromInternalCall(
                "Error de autenticación (usuario: " . $lastUsername . "): " . $error->getMessageKey()
            );
        }
        $settings = new SettingsService();
        $settings = $settings->Get($this->getDoctrine()->getRepository('AppBundle:Settings'));
        // devolvemos la página de login al usuario
        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
            'users'         => $users,
            'settings'      => $settings
        ]);
    }
}