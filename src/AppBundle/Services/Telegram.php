<?php

namespace AppBundle\Services;

class Telegram {

    private $base_url = 'https://api.telegram.org/bot';
    private $settings;

    public function __construct($settings) {
        $this->settings = $settings;
    }

    public function Send($content) {
        if ($this->settings['tg_token'] != "" && $this->settings['tg_group'] != "") {
            $ch = curl_init();
            curl_setopt_array($ch, [
                CURLOPT_URL => $this->base_url . $this->settings['tg_token'] . "/sendMessage",
                CURLOPT_POSTFIELDS => [
                    'chat_id' => $this->settings['tg_group'],
                    'text' => '<b>[ARGON]</b>
' . $content, // line break necesario
                    'parse_mode' => 'html'
                ],
                CURLOPT_RETURNTRANSFER => 1
            ]);
            $res = json_decode(curl_exec($ch), true);
            curl_close($ch);
            // return $res;
            if (isset($res['ok']) && $res['ok'] == 'true') {
                return true;
            } else {
                return false;
            }
        } else {
            return null;
        }
    }
}