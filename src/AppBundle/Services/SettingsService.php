<?php

namespace AppBundle\Services;

use AppBundle\Entity\Settings;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SettingsService {
    public function Get($em) {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $_settings = $em->findAll();
        $_settings = $serializer->serialize($_settings, 'json');
        $_settings = json_decode($_settings, true);
        $settings = [];
        foreach ($_settings as $item) {
            $settings[$item['id']] = $item['value'];
        }
        return $settings;
    }
}