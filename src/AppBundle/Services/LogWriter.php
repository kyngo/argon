<?php

namespace AppBundle\Services;

use AppBundle\Entity\Logs;

class LogWriter {
    private $em;

    public function __construct($manager) {
        $this->em = $manager;
    }

    public function FromAPICall($category, $message) { // des de la api
        $this->Write($category, $message);
    }

    public function FromInternalCall($message) { // llamadas internas
        $this->Write(2, "[ARGON] " . $message); // categoria 2: información
    }

    private function Write($category, $message) { // registra el suceso
        try {
            $log = new Logs();

            $log->setDate(new \DateTime("now"), new \DateTimeZone('Europe/Madrid'));
            $log->setCategory($category);
            $log->setMessage($message);

            $this->em->persist($log);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}