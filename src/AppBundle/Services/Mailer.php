<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Mailer {
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $templating) {
        $this->mailer     = $mailer;
        $this->templating = $templating;
    }

    public function Send($to, $subject, $content) {
        try {
            $_to = "( ";
            if (is_array($to)) {
                foreach ($to as $mail) {
                    $_to .= $mail . " ";
                }
            } else {
                $_to = $to;
            }
            $_to .= " )";
            $message = (new \Swift_Message($subject))
                ->setFrom('watchdog@kyngo.es')
                ->setTo($to)
                ->setBody(
                    $this->templating->render(
                        'mails/base.html.twig', [
                            'subject' => $subject,
                            'content' => $content,
                            'to' => $_to
                        ]
                    ),
                    'text/html'
                );

            $this->mailer->send($message);
            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}