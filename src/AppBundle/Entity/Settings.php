<?php

namespace AppBundle\Entity;

/**
 * Settings
 */
class Settings implements \Serializable
{
    /**
     * @var string
     */
    private $value;

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Settings
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    // serializado del objeto user
    public function serialize() {
        return serialize([
            $this->id,
            $this->value
        ]);
    }

    // deserializado del objeto user
    public function unserialize($serialized) {
        list(
            $this->id,
            $this->value
        ) = unserialize($serialized);
    }
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Settings
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
