#!/usr/bin/env python3

###################################
# ARGON MANAGER - MONITOR INTERNO #
###################################

# Este script se ejecutará cada 30 segundos para analizar el sistema en busca de problemas.
# Reportará el estado del SO a Argon donde procesará si hay alguna situación que deba reportarse.

# Puedes modificar los parámetros de CPU, RAM y disco a tu gusto, sobre las líneas 29, 30 y 31.

import requests     # Conexión con Argon
import json         # Archivo de configuración
import smtplib      # Enviar el correo
import base64       # Descifrar la password
import os           # Parámetro "sudo"
import sys          # Salida del programa
import time         # Sleep
import datetime     # Fecha y hora
import shutil       # Copia de ficheros
import re           # RegEx

# Variables
settings = {
    'endpoint': {
        'url': "http://projecte.kyngo.xyz/api/report", # url del endpoint, cámbiala sólo si sabes lo que haces
        'secret': '0863763fb98c2881baddb913503f012980e97674cdf149428c7597' # puedes configurar la clave de acceso en la página de configuración de Argon
    }, "comfort_zone": { # todos los parámetros que estén por debajo de ... no serán reportados
        "cpu": 90, # editable
        "ram": 90, # editable
        "storage": 90 # editable
    }, "misc": {
        "auth_log": "/var/log/auth.log"
    }
    
}

# Esta clase contendrá los datos recopilados
class storage:
    cpu = 0
    ram = 0
    disks = {}
    msg = []

# Comandos a usar para obtener parámetros del sistema
class systemCommands:
    cpu_usage = "grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'"
    ram_usage = "head -3 /proc/meminfo | tr -s ' ' | cut -f 2 -d ' ' | tr '\n' ' '"
    disk_usage = 'df -h | tail -n +2 | tr -s " " | cut -f 1,5 -d " " | grep ^/ | grep -v loop | cut -f 1 -d%'
    auth_cmp = "diff /var/log/auth.log ./auth.log"

# Añade un mensaje en el fichero de logs del agente de Argon
def log(msg):
    f_log = open("argon.log", "a")
    f_log.write(msg + "\n")
    f_log.close()
    if (len(sys.argv) == 2):
        if ("-v" in sys.argv[1]):
            print(msg)

# Controlamos el consumo de CPU de la máquina
def watchCPUUsage():
    log("Comprobando CPU...")
    cpu = float(os.popen(systemCommands.cpu_usage).read())
    storage.cpu = cpu

# Controlamos el porcentaje de RAM usado por la máquina
def watchRAMUsage():
    log("Comprobando RAM...")
    mem = os.popen(systemCommands.ram_usage).read()
    mem2 = mem.split()
    ram = float((float(mem2[2]) / float(mem2[0])) * 100)
    storage.ram = ram

# Esta función comprueba los registros de autenticación
def watchAuthFiles():
    log("Comprobando registros de autenticacion...")
    auth_log = os.popen(systemCommands.auth_cmp).read()
    auth_log2 = auth_log.split('\n')
    for line in auth_log2:
        if ("failure" in line) or ("user NOT  in sudoers" in line) or ("command not allowed" in line) and ("conversation failed" not in line): # es mal
            if (line[0] == "<"):
                storage.msg.append([4, "Intento de autenticación fallido al sistema: " + line[2:]])
    os.remove("./auth.log")
    shutil.copyfile(settings['misc']['auth_log'], './auth.log')
    
# Esta función comprueba la capacidad de los discos del sistema
def watchDisksState():
    log("Revisando estado de las particiones...")
    hdu = os.popen(systemCommands.disk_usage).read()
    hdu2 = hdu.split('\n')
    num_parts = len(hdu2) - 1
    for i in range(0, num_parts):
        i2 = hdu2[i].split()
        storage.disks.update({i2[0]: int(i2[1])})

# Esta función comprueba el estado de la red
def watchNetworkStatus():
    log("Revisando datos de red...")
    return

# Esta función reporta a Argon los problemas hallados
def sendMessage(_payload):
    log("Enviando reporte...")
    payload = "msg=" + json.dumps(storage.msg) + "&payload=" + json.dumps(_payload)
    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Cache-Control": "no-cache",
        "X-Argon-Secret": settings['endpoint']['secret']
    }

    response = requests.request(
        "POST", 
        settings['endpoint']['url'], 
        data=payload, 
        headers=headers
    )

    log(response.text)

# Llamada a funciones
try:
    log("========================\n Registro iniciado, fecha: " + str(datetime.datetime.utcnow()) + "\n------------------------")
    if (len(sys.argv) == 2):
        if ("-v" in sys.argv[1]):
            log("[INFO] Modo verboso habilitado")
    watchCPUUsage()
    watchRAMUsage()
    watchDisksState()
    watchAuthFiles()
    if os.path.isfile("./last.json"):
        # lectura de last.json
        with open("last.json", "r") as f:
            last = json.load(f)
        # log(json.dumps(last))

        # comprobación de cpu
        if (storage.cpu > settings['comfort_zone']['cpu']):
            if (last['triggered']['cpu'] == 0):
                cpu_trigger = 1
            else: 
                if (last['triggered']['cpu'] == 1):
                    storage.msg.append([3, "¡Uso de CPU por encima del " + str(settings['comfort_zone']['cpu']) + "%!"])
                    cpu_trigger = 2
                else:
                    cpu_trigger = 2
        else:
            if last['triggered']['cpu'] == 2:
                storage.msg.append([1, "El uso de CPU ha bajado al " + str(round(storage.cpu, 2)) + "%."])
            cpu_trigger = 0

        # comprobación de ram
        if (storage.ram > settings['comfort_zone']['ram']):
            if (last['triggered']['ram'] == 0):
                ram_trigger = 1
            else: 
                if (last['triggered']['ram'] == 1):
                    storage.msg.append([3, "¡Uso de RAM por encima del " + str(settings['comfort_zone']['ram']) + "%!"])
                    ram_trigger = 2
                else:
                    ram_trigger = 2
        else:
            if last['triggered']['ram'] == 2:
                storage.msg.append([1, "El uso de RAM ha bajado al " + str(round(storage.ram, 2)) + "%."])
            ram_trigger = 0

        # comprobacion de discos
        disks_trigger = {}
        for disk in storage.disks:
            if (storage.disks[disk] > settings['comfort_zone']['storage']):
                if (last['triggered']['disks'][disk] == 0):
                    disks_trigger.update({disk: 1})
                else:
                    if (last['triggered']['disks'][disk] == 1):
                        storage.msg.append([3, "¡Espacio disponible bajo en " + disk + "! (" + str(storage.disks[disk]) + "%)"])
                        disks_trigger.update({disk: 2})
                    else:
                        disks_trigger.update({disk: 2})
            else:
                if last['triggered']['disks'][disk] == 2:
                    storage.msg.append([1, "El espacio en la partición " + disk + " ha descendido a un " + str(storage.disks[disk]) + "%"])
                disks_trigger.update({disk: 0})


        

        payload = {
            "cpu": storage.cpu,
            "ram": storage.ram,
            "disks": storage.disks,
            "triggered": {
                "cpu": cpu_trigger,
                "ram": ram_trigger,
                "disks": disks_trigger,
                "su_accesses": []
            }
        }
    else:
        disks_trigger = {}
        for disk in storage.disks:
            disks_trigger.update({disk: 1})
        payload = {
            "cpu": storage.cpu,
            "ram": storage.ram,
            "disks": storage.disks,
            "triggered": {
                "cpu": 1,
                "ram": 1,
                "disks": disks_trigger,
                "su_accesses": []
            }
        }
    # log(json.dumps(payload))

    # escritura del nuevo last.json
    file = open("last.json", "w")
    file.write(json.dumps(payload))
    file.close()
    sendMessage(payload)
except KeyboardInterrupt:
    log(":: Programa cerrado por el usuario")
    sys.exit(1)
except:
    os.remove("./last.json")
    log(":: Error inesperado." + sys.exc_info()[0])
    sys.exit(2)
