#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Monitor de Sistema, por Arnau Martín
"""

# Librerías en uso
import json       # Archivo de configuración
import smtplib    # Enviar el correo
import base64     # Descifrar la password
import os         # Parámetro "sudo"
import sys        # Salida del programa
import time       # Sleep
import datetime   # Fecha y hora
import shutil     # Copia de ficheros
import re         # RegEx

# Carga la configuración del programa
with open("settings.json") as file:
    settings = json.load(file)

# Variables para el reporte informativo
class mon_vars:
    cpu_load = []
    mem_load = []
    disk_load = {}
    su_accesses = []

    def clear(self):
        mon_vars.cpu_load.clear()
        mon_vars.mem_load.clear()

# Comandos para obtener información del SO
class comandos:
    cpu_usage = "grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'"
    ram_usage = "head -2 /proc/meminfo | tr -s ' ' | cut -f 2 -d ' ' | tr '\n' ' '"
    disk_usage = 'df -h | tail -n +2 | tr -s " " | cut -f 1,5 -d " " | grep ^/ | cut -f 1 -d%'
    dir_usage = "du -sb " + settings['directory']['path'] + " | cut -f 1"
    log_usage = "du -sb " + settings['directory']['log_path'] + " | cut -f 1"
    largest_files = "find / -type f -exec du -a {} + 2> /dev/null | sort -nr | head -" + str(settings['report']['list_largest_files']) + " | tr '\t' ' '"
    large_files = "find / -type f -size +" + str(settings['report']['min_size_report']) + "M -exec du -a {} + 2> /dev/null | sort -nr | tr '\t' ' '"
    modified_files = "find / -type f -mmin -" + str(settings['report']['files_modified_in_mins']) + " 2> /dev/null | grep -v /proc" # por seguridad deshabilitamos /proc
    old_files = "find / -type f -mtime +" + str(settings['report']['files_older_than']) + " 2> /dev/null | grep -v /proc"
    empty_files = "sudo find / -type f -size 0 2> /dev/null | grep -v '^/sys' | grep -v '^/proc' | grep -v '^/usr' | grep -v '/run'"


# Genera un correo y lo manda al usuario
def mail(message, type=0):
    _type = ""
    if (type == 0):
        _type = " style='color: blue;'>Informe periodico"
    if (type == 1):
        _type = " style='color: red;'>Alerta"
    header = "From: Monitor <" + settings['mail']['from']['address'] + ">\n" \
        "To: Watchdog <" + settings['mail']['to']['address'] + ">\n" \
        "MIME-Version: 1.0\n" \
        "Content-type: text/html\n" \
        "Subject: Monitor - " + str(datetime.datetime.now()) + "\n" \
        "<h1>Monitor</h1>\n" \
        "<h2" + _type + "</h2>\n"
    try:
        server = smtplib.SMTP(settings['mail']['from']['smtp'])
        server.starttls()
        d_pass = str(base64.b64decode(settings['mail']['from']['pass']))[2:-1]
        server.login(settings['mail']['from']['address'], d_pass)
        server.sendmail(settings['mail']['from']['address'], settings['mail']['to']['address'], header + "\n" + message)
        server.quit()
    except:
        log("Ha ocurrido un error al enviar el correo. Se guardará una copia en un fichero local.")
        hora = datetime.datetime.now()
        hora2 = hora.strftime("%Y-%m-%d-%H-%M-%S")
        logtype = ""
        if (type == 0):
            logtype = "LOG"
        if (type == 1):
            logtype = "WARN"
        log_f = open(logtype + "-" + str(hora2) + ".html", "w")
        log_f.write(message)
        log_f.close()


# Imprime (si se ha pedido) el log por pantalla
def log(text):
    if (len(sys.argv) == 2):
        if ("-v" in sys.argv[1]):
            print(text)

# Función de carga del programa
def main():
    log("Iniciando monitor...")
    # Quizá se quiere que el monitor lo arranque solamente root
    a = os.getgid()
    if (settings['global']['sudo'] == 1) and a != 0:
        print("No se ha ejecutado como root! Saliendo...")
        sys.exit(1)
    if (a != 0):
        print("Aviso: Ejecutar este programa sin permisos de root puede provocar errores!")

    # Cada x tiempo se ejecuta el monitor
    # Si el contador es igual o superior al tiempo de informe,
    # envía por correo un informe del sistema y además
    # reinicia el contador
    counter = 0
    while(True):
        time.sleep(settings['global']['warn_time'])
        monitor()
        counter += settings['global']['warn_time']
        if (counter >= settings['global']['brief_time']):
            inform()
            counter = 0

# Funciones compartidas de monitorización
def dump_size():
    fs_counter = 0
    for filename in os.listdir(settings['directory']['dbdump_dir']):
        regex_dump = re.compile('.dump$')
        re_search = regex_dump.search(filename)
        if (re_search):  # coincidencia
            fs_counter += os.path.getsize(settings['directory']['dbdump_dir'] + "/" + filename)
    fs_counter = fs_counter / 1024 / 1024
    return fs_counter

# Comprueba el sistema, si ve algo contradictorio con la
# configuración, manda un correo con lo que falla
def monitor():
    log("Iniciando monitorizado...")
    msg = "<h3>Los siguientes eventos han provocado que se mande este correo:</h3><p>\n"
    trigger = 0

    # CPU
    uso_cpu = float(os.popen(comandos.cpu_usage).read())
    log("Uso de CPU: " + str(uso_cpu) + " %\n")
    mon_vars.cpu_load.append(uso_cpu)
    # Reporte de CPU
    if (int(uso_cpu) > settings['system']['max_cpu']):
        msg += "\nUso de CPU por encima de " + str(settings['system']['max_cpu']) + "% - " + str(uso_cpu) + "%<br/>\n"
        trigger += 1

    # RAM
    mem = os.popen(comandos.ram_usage).read()
    mem2 = mem.split()
    uso_mem = float(100 * float(mem2[1]) / float(mem2[0]))
    log("Uso de RAM: " + str(uso_mem) + " %\n")
    mon_vars.mem_load.append(uso_mem)
    # Reporte de RAM
    if (int(uso_mem) > settings['system']['max_ram']):
        msg += "\nUso de RAM por encima de " + str(settings['system']['max_ram']) + "% - " + str(uso_mem) + "%<br/>\n"
        trigger += 1

    # Uso de discos
    hdu = os.popen(comandos.disk_usage).read()
    hdu2 = hdu.split('\n')
    num_parts = len(hdu2) - 2
    for i in range(0, num_parts):
        i2 = hdu2[i].split()
        mon_vars.disk_load.update({i2[0]: 100 - int(i2[1])})
        # Reporte de uso de discos
        if (int(mon_vars.disk_load[i2[0]]) < settings['system']['space_free']):
            msg += "\nAlmacenamiento en "  + str(i2[0]) + " por debajo del " + str(settings['system']['space_free']) + "% - Libre: " + str(mon_vars.disk_load[i2[0]]) + "%<br/>\n"
            trigger += 1
    log("Uso de discos (en GB): " + str(mon_vars.disk_load))

    # Monitorizado de directorio concreto
    dir = int(os.popen(comandos.dir_usage).read())
    uso_dir = int(dir / 1024 / 1024)
    log("Espacio usado en " + settings['directory']['path'] + ": " + str(uso_dir) + " MB")
    # Reporte del directorio
    if (int(uso_dir) > settings['directory']['max_size']):
        msg += "\nAlmacenamiento en " + settings['directory']['path'] + " por encima de " + str(settings['directory']['max_size']) + " MB - " + str(uso_dir) + " MB<br/>\n"
        trigger += 1

    # Copia de logs del sistema
    logdir = int (os.popen(comandos.log_usage).read())
    uso_logdir = int(logdir / 1024 / 1024)
    log("Espacio usado en " + settings['directory']['log_path'] + ": " + str(uso_logdir) + " MB")
    # Reporte de logs del sistema
    if (int(uso_logdir) > settings['directory']['max_log_size']):
        msg += "\nAlmacenamiento en " + settings['directory']['log_path'] + " por encima de " + str(settings['directory']['max_log_size']) + " MB - " + str(uso_logdir) + " MB<br/>\n"
        msg += "Los ficheros han sido copiados al directorio de backups, y posteriormente han sido todos vaciados.<br/>\n"
        trigger += 1
        logpath = settings['directory']['log_backup_path'] + "/" + str(datetime.datetime.now()) + "/"
        os.makedirs(logpath)
        for filename in os.listdir(settings['directory']['log_path']):
            if (os.path.isfile(settings['directory']['log_path'] + "/" + filename)):
                shutil.copy2(settings['directory']['log_path'] + "/" + filename, logpath)
                _f = open(settings['directory']['log_path'] + "/" + filename, "w")
                _f.close()

    # Cuenta el numero de ficheros en un directorio
    f_counter = 0
    for filename in os.listdir(settings['directory']['count_dir_path']):
        if (os.path.isfile(settings['directory']['count_dir_path'] + "/" + filename)):
            f_counter += 1
    log("Ficheros en " + settings['directory']['count_dir_path'] + ": " + str(f_counter))
    if (f_counter > settings['directory']['max_files']):
        trigger += 1
        msg += "\nEl directorio " + settings['directory']['count_dir_path'] + " contiene " + str(f_counter) + " archivos, supera el maximo de " + str(settings['directory']['max_files']) + "!<br/>\n"

    # Calcula el espacio de ficheros con extensión determinada
    fs_counter = dump_size()
    log("Espacio ocupado por los ficheros .dump: " + str(fs_counter) + " MB")
    # Reporte del tamaño de ficheros .dump
    if (fs_counter > settings['directory']['max_dbdump_size']):
        trigger += 1
        msg += "\nLos ficheros .dump en el directorio " + settings['directory']['dbdump_dir'] + " ocupan mas de " + str(settings['directory']['max_dbdump_size']) + " MB - En concreto " + str(fs_counter) + " MB!<br/>\n"

    # Comprobar autenticación
    auth_file = open("/var/log/auth.log", "r")
    clr_log = 0
    for line in auth_file:
        # Reporte y acciones por autenticaciones fallidas
        if ("failure" in line) or ("session opened" in line) or ("Successful" in line):
            mon_vars.su_accesses.append(line)
            if ("failure" in line) or ("user NOT  in sudoers" in line) or ("command not allowed" in line):
                trigger += 1
                clr_log += 1
                msg += "\nAUTH: Fallido inicio de sesion. Detalles:<br/>" + line + "<br/>\n"
    auth_file.close()
    if (clr_log > 0):
        auth_file = open("/var/log/auth.log", "w")
        auth_file.close()
    log("Intentos fallidos de autenticación: " + str(clr_log))

# Comprueba si se ha activado algún trigger, si es así manda el correo
    if (trigger > 0):
        msg += "</p>"
        mail(msg, type=1)
    log("-----")

# Hace un reporte cada x tiempo del sistema y lo manda al sysadmin
def inform():
    log("Iniciando informe...")
    msg = "<p>"

    # Uso medio de CPU y RAM
    log("Uso medio de CPU y RAM:")
    avg_cpu = sum(mon_vars.cpu_load) / len(mon_vars.cpu_load)
    print("CPU: " + str(avg_cpu) + " %")
    avg_ram = sum(mon_vars.mem_load) / len(mon_vars.mem_load)
    print("RAM: " + str(avg_ram) + " %")
    msg += "<h3>Uso medio de CPU: " + str(avg_cpu) + " %</h3>"
    msg += "<h3>Uso medio de RAM: " + str(avg_ram) + " %</h3>"

    # Autenticaciones realizadas
    log("Autenticaciones realizadas:")
    msg += "<h3>Autenticaciones realizadas:</h3><br/>\n"
    for linea in mon_vars.su_accesses:
        log(linea)
        msg += linea + "<br/>\n"


    # Registrando ficheros más grandes del sistema
    l_files = os.popen(comandos.largest_files).read()
    l_files2 = str(l_files).split("\n")
    dict_files = {}
    for element in l_files2:
        a = element.split(" ")
        _buffer = ""
        for i in range (1, len(a)):
            _buffer += a[i] + " "
        if not (_buffer == ""):
            dict_files.update({_buffer: int(a[0]) / 1024})
    # Informe de ficheros más grandes del sistema
    log("Ficheros más grandes en el sistema:")
    msg += "<h3>Ficheros mas grandes del sistema:</h3><br/>\n"
    for index,key in dict_files.items():
        log(index + ": " + str(key) + " MB")
        msg += index + ": " + str(key) + " MB<br/>\n"
    # Espacio ocupado por los ficheros .dump
    fs_counter = dump_size()
    log("Espacio ocupado por los ficheros .dump: " + str(fs_counter) + " MB")
    msg += "<h3>Espacio ocupado por los ficheros .dump: " + str(fs_counter) + " MB</h3>"
    # Reportar ficheros más grandes de N megabytes
    largef = os.popen(comandos.large_files).read()
    largef2 = str(largef).split("\n")
    dict_size = {}
    for element in largef2:
        a = element.split(" ")
        _buffer = ""
        for i in range(1, len(a)):
            _buffer += a[i] + " "
        if not (_buffer == ""):
            dict_size.update({_buffer: int(a[0]) / 1024})
    log("Ficheros más grandes de " + str(settings['report']['min_size_report']) + " MB en el sistema:")
    msg += "<br/><h3>Ficheros mas grandes de " + str(settings['report']['min_size_report']) + " MB del sistema:</h3><br/>\n"
    for index, key in dict_size.items():
        log(index + ": " + str(key) + " MB")
        msg += index + ": " + str(key) + " MB<br/>\n"
    # Ficheros modificados en el último n tiempo
    modded_f = os.popen(comandos.modified_files).read()
    modded_f2 = modded_f.split('\n')
    arr_mod_f = []
    log("Ficheros modificados en los últimos " + str(settings['report']['files_modified_in_mins']) + " minutos:")
    msg += "<br/><h3>Ficheros modificados en los ultimos " + str(settings['report']['files_modified_in_mins']) + " minutos:</h3><br/>\n"
    for i in modded_f2:
        arr_mod_f.append(i)
        log(i)
        msg += i + "<br/>\n"

    # Ficheros más viejos de n tiempo
    old_f = os.popen(comandos.old_files).read()
    old_f2 = old_f.split('\n')
    arr_old_f = []
    log("Ficheros mas viejos de " + str(settings['report']['files_older_than']) + " dias:")
    msg += "<br/><h3>Ficheros mas viejos de " + str(settings['report']['files_older_than']) + " dias:</h3><br/>\n"
    for i in old_f2:
        arr_old_f.append(i)
        log(i)
        msg += i + "<br/>\n"

    # Ficheros vacios
    empty_f = os.popen(comandos.empty_files).read()
    empty_f2 = empty_f.split('\n')
    arr_empty_f = []
    log("Ficheros vacios en el sistema de ficheros:")
    msg += "<br/><h3>Ficheros vacios en el sistema de ficheros:</h3><br/>\n"
    for i in empty_f2:
        arr_empty_f.append(i)
        log(i)
        msg += i + "<br/>\n"

    msg += "</p>"
    log("----")
    mail(msg)

# Inicia el programa
try:
    # Comprueba que sea Linux el SO que lo ejecuta
    operating = os.popen('uname').read()
    if (operating == "Linux\n"):
        log("[INFO] Detectado sistema operativo GNU/Linux!")
    else: # Sale si no lo es
        log("Error! Sistema incompatible. Saliendo...")
        sys.exit(1)
    main() # Inicia el programa
except KeyboardInterrupt: # Ctrl+C para cerrar la app
    log("Saliendo...")
    sys.exit(0)