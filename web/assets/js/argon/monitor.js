var cores = 0;
var ram = 0;
var swap = 0;
var disks_ammount = 0;
var config = null;
var cpu_chart = null;
var ram_chart = null;
var disk_charts = [];
var time = null;
var time_counter = 0;
var config = {
    "hideCredits": true, // amCharts confidential (tm)
    "type": "serial",
    "theme": "light",
    "dataDateFormat": "YYYY-MM-DD HH:NN:SS",
    "legend": {
        "useGraphSettings": true
    },
    "dataProvider": [],
    "synchronizeGrid":false,
    "valueAxes": [],
    "graphs": [],
    "categoryField": "date",
    "categoryAxis": {
        "gridThickness": 0,
        "parseDates": true,
        "minPeriod": "ss",
        "axisColor": "#DADADA",
        "color": "#ffffff",
        "minorGridEnabled": true
    },"categoryAxesSettings": {
        "maxSeries": 1,
        "groupToPeriods": ["YYYY-MM-DD HH:NN:SS"]
    }, "export": {
        "enabled": true,
        "position": "bottom-right",
        "fileName": "argon"
    },"listeners": [{
        "event": "init",
        "method": function(e) {
            e.chart.removeListener(e.chart.chartScrollbar, "zoomed", e.chart.handleScrollbarZoom);
        }
    }]
};

window.onload = async function() {
    getSystemSettings();
}

function getSystemSettings() {
    $.ajax({
        url: "./monitor/settings.json",
        type: "POST",
        success: function(data) {
            cores = data.cores;
            ram = data.ram;
            swap = data.swap;
            disks_ammount = data.disks;
            if (data.public_ip == null) {
                data.public_ip = "sin conexión a internet";
            }
            $("#public_ip").html(data.public_ip);
            cpu_config = generateCPUGraphConfig();
            cpu_chart = AmCharts.makeChart("graph_cpu", cpu_config);
            ram_config = generateRAMGraphConfig();
            ram_chart = AmCharts.makeChart("graph_ram", ram_config);
            time = moment().format("YYYY-MM-DD HH:mm:ss");
            $("#graphs_disks").empty();
            for (var idx = 0; idx < disks_ammount; idx++) {
                $("#graphs_disks").append(
                    "<div id=\"graph_disk_" + idx + "\" class=\"col-md-3 col-sm-12 chart-sm\"></div>"
                );
                disk_charts.push(
                    AmCharts.makeChart("graph_disk_" + idx, clone({
                        "hideCredits": true, // amCharts confidential (tm)
                        "type": "pie",
                        "theme": "light",
                        "titles": [{
                            "text": "...",
                            "bold": false
                        }],/*
                        "allLabels": [{
                            "y": "50%",
                            "align": "center",
                            "size": 14,
                            "bold": false,
                            "text": "...",
                            "color": "#555"
                        }],*/
                        "dataProvider": [{
                            "title": "Usado",
                            "value": 50,
                            "color": "#F0A147"
                        }, {
                            "title": "Libre",
                            "value": 50,
                            "color": "#2482D1"
                        }],
                        "titleField": "title",
                        "valueField": "value",
                        "colorField": "color",
                        "labelRadius": -50,
                        "radius": "42%",
                        "innerRadius": "0%",
                        "export": {
                            "enabled": true
                        }
                    }))
                );
                disk_charts[idx].validateData();
            }
            getData();
        }, error: function(data) {
            toastr.error(
                "Ha ocurrido un error crítico al intentar obtener los datos del sistema."
            );
        }
    });
    return true;
}  

function getData() {
    $.ajax({
        url: "./monitor/get.json",
        type: "POST",
        success: function(data) {
            // cpu
            var cpu = data.cpu_usage;
            var cpu_stats = {
                "date": moment(time, "YYYY-MM-DD HH:mm:ss").add(time_counter, 'seconds').format("YYYY-MM-DD HH:mm:ss")
            };
            var counter = 0;
            Object.keys(cpu).forEach(function(idx) {
                cpu_stats["core_" + counter] = Math.round(cpu[idx].user + cpu[idx].sys);
                counter++;
            });
            cpu_chart.dataProvider.push(cpu_stats);
            if (cpu_chart.dataProvider.length > 60) {
                cpu_chart.dataProvider.shift();
            }
            cpu_chart.validateData();
            // ram
            var ram_stats = {
                "date": moment(time, "YYYY-MM-DD HH:mm:ss").add(time_counter, 'seconds').format("YYYY-MM-DD HH:mm:ss"),
                "ram": Math.round((data.ram.used / ram) * 100),
                "swap": Math.round((data.ram.swap_used / swap) * 100)
            };
            ram_chart.dataProvider.push(ram_stats);
            if (ram_chart.dataProvider.length > 60) {
                ram_chart.dataProvider.shift();
            }
            ram_chart.validateData();
            // discos
            Object.keys(data.disks).forEach(function(idx) {
                var dt = data.disks[idx].used + data.disks[idx].free;
                if (disk_charts[idx].dataProvider[0]['value'] != data.disks[idx].used) { // verificamos que se ha modificado
                    disk_charts[idx].dataProvider = [ {
                        "title": "Usado",
                        "value": data.disks[idx].used,
                        "color": "#F0A147"
                    }, {
                        "title": "Libre",
                        "value": data.disks[idx].free,
                        "color": "#2482D1"
                    } ];
                    disk_charts[idx].titles = [{
                            "text": data.disks[idx].mountpoint,
                            "bold": false
                        }];
                    disk_charts[idx].validateData();
                }
            });

            // red
            $("#network_interfaces").empty();
            data.network.forEach(function(idx) {
                if (idx.ipv4 == "") { idx.ipv4 = "---" }
                if (idx.ipv6 == "") { idx.ipv6 = "---" }
                $("#network_interfaces").append(
                    "<tr>" +
                    "<td>" + idx.interface + "</td>" +
                    "<td>" + idx.ipv4 + "</td>" +
                    "<td>" + idx.ipv6 + "</td>" +
                    "</tr>"
                );
            });
            // fin de la logica
            time_counter++;
            getData();
        }, error: function() {
            toastr.error(
                "Ha ocurrido un error al obtener la información del sistema. <br/>Por favor, recarga la página, y asegúrate de que el servidor tiene conexión contigo."
            );
        }
    });
}

function generateCPUGraphConfig() {
    var c = clone(config);
    for (var idx = 0; idx < cores; idx++) {
        c.graphs.push({
            "bulletBorderThickness": 1,
            "lineThickness": 2,
            "hideBulletsCount": 30,
            "title": "Core #" + idx,
            "valueField": "core_" + idx,
            "fillAlphas": 0
        });
    }
    c.valueAxes = [{
        "maximum": 100,
        "minimum": 0,
        "min": 0,
        "max": 100,
        "unit": "%",
        "strictMinMax": true,
        "gridCount": 10
    }];
    return c;
}

function generateRAMGraphConfig() {
    var c = clone(config);
    c.graphs.push({
        "bulletBorderThickness": 1,
        "lineThickness": 5,
        "hideBulletsCount": 30,
        "title": "Uso de RAM",
        "valueField": "ram",
        "fillAlphas": 0
    });
    if (swap > 0) {
        c.graphs.push({
            "bulletBorderThickness": 1,
            "lineThickness": 5,
            "type": "smoothedLine",
            "hideBulletsCount": 30,
            "title": "Uso de Swap",
            "valueField": "swap",
            "fillAlphas": 0
        });
    }
    c.valueAxes = [{
        "maximum": 100,
        "minimum": 0,
        "min": 0,
        "max": 100,
        "unit": "%",
        "strictMinMax": true
    }];
    return c;
}

function clone(obj) {
    var copy;
    if (null == obj || "object" != typeof obj) return obj;
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }
    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }
    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
        }
        return copy;
    }
    throw new Error("No se pudo realizar correctamente el objeto deseado.");
}
